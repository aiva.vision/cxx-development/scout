# Scout

The app featured in the [Building a Real-Time Bridge with Kafka](https://forum.confluent.io/t/recording-ready-to-view-speaker-q-a-thread-build-a-real-time-bridge-with-kafka-to-the-cloud-8-march-2022/4298) meetup with Confluent.

Scout analyzes a video stream of your choice and extracts information about the detected entities. The generated metadata is sent to the local Kafka instance.

### Dependencies

- [CMake](https://cmake.org/install/)
- [ViFLow](https://gitlab.com/aiva.vision/cxx-development/viflow)
- [GStreamer](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c)
- [GLib](https://developer.gnome.org/glib/)
- [GObject](https://developer.gnome.org/gobject/)
- [nlohman_json](https://github.com/nlohmann/json)

To install some of the prerequisites, execute the following command:

```bash
sudo apt-get install \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev \
    libgstrtspserver-1.0-dev \
    libgstreamer1.0-dev
```

For the rest, try using the scripts from [this repository](https://gitlab.com/aiva.vision/devops/packages).

### Apache Zookeper & Kafka

This app connects to a Kafka cluster running on the same host. Use the following commands to start Zookeeper and Kafka.

```bash
docker run -d --rm -it --net=host --name=zookeeper \
-e ZOO_DATA_DIR=/opt/zookeeper/data \
-e ZOO_DATA_LOG_DIR=/opt/zookeeper/datalog \
-v /opt/zookeeper/data:/opt/zookeeper/data \
-v /opt/zookeeper/datalog:/opt/zookeeper/datalog \
zookeeper:<tag>
```

```bash
docker run -d --rm --net=host --name=kafka \
-e KAFKA_ZOOKEEPER_CONNECT=localhost:2181 \
-e KAFKA_LISTENERS=PLAINTEXT://localhost:9092 \
-e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:9092 \
-e KAFKA_LOG_DIRS=/opt/kafka/logs \
-v /opt/kafka/logs:/opt/kafka/logs \
registry.gitlab.com/aiva.vision/devops/packages/kafka:<tag>
```

Verify that the containers are running with:

```bash
docker container ls
```

The containers can be stopped with:

```bash
docker container stop zookeeper
docker container stop kafka
```

### Install

1. Clone the repository.

   ```bash
   git clone git@gitlab.com:aiva.vision/cxx-development/scout.git
   ```

2. Build the project and install:

   ```bash
   mkdir -p scout/release

   cmake -S scout -B scout/release -D CMAKE_BUILD_TYPE=Release

   cmake --build scout/release -j

   cmake --install scout/release
   ```

### Run

The repository contains some sample footage with the appropriate configs. The configs use some pre-trained models that are expected to be in the path specified in the config. The configs can be modified accordingly for the host, but it is easier to copy them directly to the expected location.

```bash
sudo cp -r ./models/* /opt/nvidia/deepstream/deepstream/samples/models
```

Finally, execute the binary:

```bash
scout ./configs/<usecase>/main.cfg
```
