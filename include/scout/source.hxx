#pragma once

#include <viflow/bin.hxx>

namespace Scout {

class Source : public viflow::Bin {
public:
  /*
     No cleanup is needed.
     */
  ~Source() {}
  /*
     Sets the uri for the uridecodebin.
     */
  void set_uri(const std::string &uri);

private:
  /*
     Creates a named element.
     */
  Source(const std::string &name);

  friend class viflow::Element;
};

} // namespace Scout
