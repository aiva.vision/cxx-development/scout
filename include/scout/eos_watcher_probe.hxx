#pragma once

#include <memory>
#include <mutex>
#include <viflow/pad_probe.hxx>

namespace Scout {

/*
   Resets the source if an EOS event is received. This enables file looping.
   */
class EOS_Watcher_Probe : public viflow::Pad_Probe {
public:
  EOS_Watcher_Probe() {}

  ~EOS_Watcher_Probe() {}

  GstPadProbeReturn process(GstPad *pad, GstPadProbeInfo *info);

  GstPadProbeType get_type() const noexcept;
};

} // namespace Scout
