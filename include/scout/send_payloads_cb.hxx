#pragma once

#include <gstnvdsmeta.h>
#include <libconfig.h++>
#include <librdkafka/rdkafkacpp.h>
#include <nlohmann/json.hpp>

#include "viflow/app_sink_callback.hxx"

namespace scout {

class Send_Payloads_Cb : public viflow::New_Sample_Callback {
public:
  Send_Payloads_Cb();

  ~Send_Payloads_Cb();
  /*
     Sends a message to the Kafka cluster.
     */
  GstFlowReturn process(viflow::Shared_Element &app_sink);

private:
  /*
     Parses the parameters from the config and initializes the producer.

     May throw: std::runtime_error
     */
  void create_producer();
  /*
     May throw: std::runtime_error
     */
  void send(const std::string &payload) const;
  /*
     May throw: std::runtime_error
     */
  std::vector<nlohmann::json> generate_payloads(NvDsBatchMeta *meta) const;
  /*
     Topic to send the messages to.
     */
  std::string topic;
  /*
     Handle to a Kafka producer.
     */
  std::unique_ptr<RdKafka::Producer> producer;
};

} // namespace scout
