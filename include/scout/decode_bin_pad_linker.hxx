#pragma once

#include <viflow/element_callback.hxx>

namespace Scout {

class Decode_Bin_Pad_Linker : public viflow::Pad_Added_Callback {
public:
  Decode_Bin_Pad_Linker(viflow::Shared_Element &video_target);

  void process_pad(viflow::Shared_Element &elem, viflow::Pad &new_pad);

private:
  /*
     Element to link to.
     */
  viflow::Shared_Element video_elem;
};

} // namespace Scout
