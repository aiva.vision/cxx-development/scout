#pragma once

#include <filesystem>
#include <future>
#include <libconfig.h++>
#include <viflow/pipeline.hxx>

namespace Scout {

class Pipeline : public viflow::Pipeline {
public:
  static const std::string &name;
  /*
     Registers a pipeline built from the config.
     Returns a pointer to the registered pipeline.
     */
  static viflow::Shared_Element &
  register_instance(const std::filesystem::path &config);

  static viflow::Shared_Element &get_instance();
  /*
     The cleanup is handled by the base class.
     */
  ~Pipeline() {}
  /*
     Resets the source element.
     */
  void reset_source();

private:
  /*
     Builds the pipeline from the config.
     */
  Pipeline(const std::filesystem::path &config);
  /*
     Adds the source element.
     */
  std::shared_ptr<viflow::Element> create_source(const std::string &uri);
  /*
     Adds the source element.
     */
  void add_source(const libconfig::Setting &config);
  /*
     Adds the source element.
     */
  void destroy_source();
  /*
     Appends the stream muxer to the pipeline.
     */
  void append_stream_muxer(const libconfig::Setting &config);
  /*
     Appends the inference engine to the pipeline.
     */
  void append_inference_engine(const std::filesystem::path &config);
  /*
     Appends the tracker to the pipeline.
     */
  void append_tracker(const std::filesystem::path &config);
  /*
     Appends the analytics to the pipeline.
     */
  void append_analytics(const std::filesystem::path &config);
  /*
     Adds the multi sink element.
     */
  void append_sinks();
  /*
     Used later if the source is resetted.
     */
  std::filesystem::path config_file;
  /*
     Task that recreates the source.
     */
  std::future<void> recreate_task;

  friend class viflow::Element;
};

} // namespace Scout
