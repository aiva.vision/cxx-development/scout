#pragma once

#include <viflow/bus_watch.hxx>

namespace Scout {

class Bus_Listener : public viflow::Bus_Watch {
public:
  bool process_message(const viflow::Bus &bus, const viflow::Message &message);

  void process_info(const viflow::Message &message);

  void process_warning(const viflow::Message &message);

  void process_error(const viflow::Message &message);

  void process_state_changed(const viflow::Message &message);

  void process_eos(const viflow::Message &message);
};

} // namespace Scout
