#pragma once

#include <viflow/child_proxy_callback.hxx>

namespace Scout {

class Decode_Bin_Child_Cb : public viflow::Child_Proxy_Callback {
public:
  void process(viflow::Shared_Element &proxy, GObject *child, char *name);
};

} // namespace Scout
