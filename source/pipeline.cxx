#include <future>
#include <memory>
#include <spdlog/spdlog.h>
#include <viflow/app_sink.hxx>
#include <viflow/tee.hxx>
#include <viflow/uri_decode_bin.hxx>

#include "scout/bus_listener.hxx"
#include "scout/eos_watcher_probe.hxx"
#include "scout/pipeline.hxx"
#include "scout/send_payloads_cb.hxx"
#include "scout/source.hxx"

static std::string get_abs_path(const libconfig::Setting &setting,
                                const std::string &field) {

  std::filesystem::path p;
  p = setting[field];

  if (p.is_absolute()) {
    return p;
  }
  auto config_path = std::filesystem::absolute(setting.getSourceFile());
  return config_path.parent_path() / p;
}

const std::string &Scout::Pipeline::name = "scout_pipeline";

viflow::Shared_Element &
Scout::Pipeline::register_instance(const std::filesystem::path &config) {

  return viflow::Element::register_instance<Scout::Pipeline>(config);
}

viflow::Shared_Element &Scout::Pipeline::get_instance() {
  return viflow::Element::get_instance(name);
}

Scout::Pipeline::Pipeline(const std::filesystem::path &config)
    : viflow::Pipeline(name), config_file{config} {
  /*
     Parse the config file.
     */
  libconfig::Config cfg;
  try {
    cfg.readFile(config.c_str());

  } catch (const libconfig::FileIOException &e) {

    throw std::runtime_error{
        std::string{"Failed to open the config file from: "} + config.string()};
  }

  libconfig::Setting &c = cfg.getRoot();

  auto inference_engine_config = get_abs_path(c, "inference-engine-config");
  auto tracker_config = get_abs_path(c, "tracker-config");
  auto analytics_config = get_abs_path(c, "analytics-config");
  /*
     Add and configure the elements.
     */
  add_source(c["stream"]);
  append_stream_muxer(c["stream"]);
  append_inference_engine(inference_engine_config);
  append_tracker(tracker_config);
  append_analytics(analytics_config);
  append_sinks();
  /*
     Register a bus watch.
     */
  get_bus()->add_watch(std::make_unique<Bus_Listener>());
}

void Scout::Pipeline::add_source(const libconfig::Setting &config) {

  auto src_uri = "file://" + get_abs_path(config, "file");

  auto src = add<Source>("scout_src");
  src->cast<Source>()->set_uri(src_uri.c_str());
}

void Scout::Pipeline::append_stream_muxer(const libconfig::Setting &config) {

  int width = config["width"];
  int height = config["height"];

  auto muxer = add("nvstreammux", "stream_muxer");

  muxer->set_property("batch-size", 1);
  muxer->set_property("width", width);
  muxer->set_property("height", height);
  muxer->set_property("nvbuf-memory-type", 0);
  muxer->set_property("batched-push-timeout", 40000);
  /*
     Add a probe to watch for EOS events.
     */
  auto &muxer_pad = muxer->get_pad("sink_0");
  muxer_pad.add_probe(std::make_unique<EOS_Watcher_Probe>());
  /*
     Connect the source to the muxer.
     */
  auto src = get_element("scout_src");
  src->get_pad("src").link(muxer_pad);
}

void Scout::Pipeline::append_inference_engine(
    const std::filesystem::path &config) {

  auto queue = append("queue", "inference_engine_queue");
  auto engine = append("nvinfer", "inference_engine");

  engine->set_property("batch-size", 1);
  engine->set_property("config-file-path", config.c_str());
}

void Scout::Pipeline::append_tracker(const std::filesystem::path &config) {

  auto queue = append("queue", "tracker_queue");
  auto tracker = append("nvtracker", "tracker");

  tracker->set_property(
      "ll-lib-file",
      "/opt/nvidia/deepstream/deepstream/lib/libnvds_nvmultiobjecttracker.so");
  tracker->set_property("ll-config-file", config.c_str());
}

void Scout::Pipeline::append_analytics(const std::filesystem::path &config) {

  auto queue = append("queue", "analytics_queue");
  auto analytics = append("nvdsanalytics", "analytics");

  analytics->set_property("config-file", config.c_str());
}

void Scout::Pipeline::append_sinks() {

  auto tee = append<viflow::Tee>("sinks_tee");

  append("queue", "osd_queue");
  append("nvdsosd", "osd");
  append("nvvideoconvert", "nv_video_converter");

  auto egl_sink = append("glimagesink", "gl_sink");

  egl_sink->set_property("enable-last-sample", false);
  egl_sink->set_property("sync", true);
  egl_sink->set_property("qos", false);
  return;

  auto metadata_queue = add("queue", "metadata_queue");
  auto metadata_sink = append<viflow::App_Sink>("metadata_sink");

  metadata_sink->set_property("enable-last-sample", false);
  metadata_sink->set_property("sync", false);
  metadata_sink->set_property("qos", false);
  metadata_sink->set_property("emit-signals", true);

  metadata_sink->cast<viflow::App_Sink>()->connect_callback(
      std::make_unique<scout::Send_Payloads_Cb>());

  tee->link(metadata_queue);
}

void Scout::Pipeline::destroy_source() {

  auto source = get_element("scout_src");

  remove("scout_src");
  source->stop();
  source->unregister();
  /*
     At this point, there should be no other reference to the element in use.
     */
  if (!source.unique()) {
    throw std::runtime_error{"The source should be unique at this point"};
  }
}

void Scout::Pipeline::reset_source() {
  recreate_task = std::async(std::launch::async, [this]() {
    try {

      destroy_source();

      /*
         Parse the config file.
         */
      libconfig::Config cfg;
      try {
        cfg.readFile(config_file.c_str());

      } catch (const libconfig::FileIOException &e) {

        throw std::runtime_error{
            std::string{"Failed to open the config file from: "} +
            config_file.string()};
      }

      libconfig::Setting &c = cfg.getRoot();
      add_source(c["stream"]);

      link_elements("scout_src", "src", "stream_muxer", "sink_0");

      auto source = get_element("scout_src");

      if (!source->sync_state_with_parent()) {
        spdlog::error("Failed to sync the source's state with the parrent's.");
      } else {
        spdlog::info("The source has been reset successfully.");
        return;
      }
    } catch (const std::exception &e) {
      spdlog::error("Caught an exception when recreating the source: {}",
                    e.what());
    } catch (...) {
      spdlog::error("Caught an unknown exception when recreating the source.");
    }
  });
}
