#include <glib-object.h>
#include <gst/gstchildproxy.h>
#include <viflow/uri_decode_bin.hxx>

#include "scout/decode_bin_child_cb.hxx"

static void configure_decoder(GstChildProxy *self, GObject *child, gchar *name,
                              viflow::Child_Proxy_Callback *callback) {

  if ((g_strrstr(name, "h264parse") == name) ||
      (g_strrstr(name, "h265parse") == name)) {
    g_object_set(child, "config-interval", -1, nullptr);
  }
  if (g_strrstr(name, "fakesink") == name) {
    g_object_set(child, "enable-last-sample", false, nullptr);
  }
  if (g_strstr_len(name, -1, "nvjpegdec") == name) {
    g_object_set(child, "DeepStream", true, nullptr);
  }
  if (g_strstr_len(name, -1, "nvv4l2decoder") == name) {
#ifdef TEGRA_PLATFORM
    g_object_set(child, "enable-max-performance", true, nullptr);
#else
    g_object_set(child, "gpu-id", 0, nullptr);
    g_object_set(child, "cudadec-memtype", 0, nullptr);
#endif
    g_object_set(child, "drop-frame-interval", 0, nullptr);
    g_object_set(child, "num-extra-surfaces", 0, nullptr);
  }
}

void Scout::Decode_Bin_Child_Cb::process(viflow::Shared_Element &proxy,
                                         GObject *child, char *name) {

  if (g_str_has_prefix(name, "decodebin")) {
    g_signal_connect(child, "child-added", G_CALLBACK(configure_decoder), NULL);
  }
}
