#include <chrono>
#include <gstnvdsmeta.h>
#include <nvds_analytics_meta.h>
#include <viflow/app_sink.hxx>

#include "scout/send_payloads_cb.hxx"

static const std::chrono::milliseconds g_max_flush_timeout(3 * 1000);
static const std::chrono::milliseconds g_max_poll_timeout(1 * 1000);

scout::Send_Payloads_Cb::Send_Payloads_Cb() : topic{"scout"} {
  create_producer();
}

scout::Send_Payloads_Cb::~Send_Payloads_Cb() {
  producer->flush(g_max_flush_timeout.count());
}

void scout::Send_Payloads_Cb::create_producer() {

  std::unique_ptr<RdKafka::Conf> conf{
      RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL)};

  std::string errstr;
  conf->set("bootstrap.servers", "localhost:9092", errstr);

  producer.reset(RdKafka::Producer::create(conf.get(), errstr));
  if (!producer) {
    throw std::runtime_error{std::string{"Failed to create producer: "} +
                             errstr};
  }
}

void scout::Send_Payloads_Cb::send(const std::string &payload) const {

  RdKafka::ErrorCode err = RdKafka::ERR_UNKNOWN;

  while (err != RdKafka::ERR_NO_ERROR) {

    err = producer->produce(
        /* Topic name */
        topic,
        /*
           Any Partition: the builtin partitioner will be used to assign the
           message to a topic based on the message key, or random partition if
           the key is not set.
           */
        RdKafka::Topic::PARTITION_UA,
        /* Make a copy of the value */
        RdKafka::Producer::RK_MSG_COPY /* Copy payload */,
        /* Value */
        const_cast<char *>(payload.c_str()), payload.size(),
        /* Key */
        nullptr, 0,
        /* Timestamp (defaults to current time) */
        0,
        /* Per-message opaque value passed to delivery report. */
        nullptr);

    if (err != RdKafka::ERR_NO_ERROR) {

      if (err == RdKafka::ERR__QUEUE_FULL) {
        /*
           If the internal queue is full, wait for messages to be delivered and
           then retry.  The internal queue is limited by the configuration
           property queue.buffering.max.messages
           */
        producer->poll(g_max_poll_timeout.count());
      } else {
        throw std::runtime_error{std::string{"Failed to produce to topic "} +
                                 topic + ": " + RdKafka::err2str(err)};
      }
    }
  }
  producer->poll(0);
}

GstFlowReturn
scout::Send_Payloads_Cb::process(viflow::Shared_Element &app_sink) {

  auto actual_app_sink = app_sink->cast<viflow::App_Sink>();
  auto sample = actual_app_sink->get_sample();
  auto buf = sample.get_buffer();

  NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta(buf.get_core());

  for (const auto &payload : generate_payloads(batch_meta)) {
    send(payload.dump());
  }

  return GST_FLOW_OK;
}

const unsigned int g_max_time_stamp_length = 64;

static std::string generate_timestamp() {
  time_t tloc;
  struct tm tm_log;
  struct timespec ts;
  char strmsec[6]; //.nnnZ\0

  auto timestamp = std::make_unique<char[]>(g_max_time_stamp_length + 1);

  clock_gettime(CLOCK_REALTIME, &ts);
  memcpy(&tloc, (void *)(&ts.tv_sec), sizeof(time_t));
  gmtime_r(&tloc, &tm_log);
  strftime(timestamp.get(), g_max_time_stamp_length, "%Y-%m-%dT%H:%M:%S",
           &tm_log);
  int ms = ts.tv_nsec / 1000000;
  snprintf(strmsec, sizeof(strmsec), ".%.3dZ", ms);
  strncat(timestamp.get(), strmsec, g_max_time_stamp_length);

  return std::string{timestamp.get()};
}

std::vector<nlohmann::json>
scout::Send_Payloads_Cb::generate_payloads(NvDsBatchMeta *meta) const {

  std::vector<nlohmann::json> payloads;
  const auto timestamp = generate_timestamp();

  for (auto l_frame = meta->frame_meta_list; l_frame; l_frame = l_frame->next) {

    auto frame_meta = static_cast<NvDsFrameMeta *>(l_frame->data);

    if (!frame_meta) {
      continue;
    }
    for (auto l_user = frame_meta->frame_user_meta_list; l_user != nullptr;
         l_user = l_user->next) {

      auto user_meta = static_cast<NvDsUserMeta *>(l_user->data);

      if (user_meta->base_meta.meta_type ==
          NVDS_USER_FRAME_META_NVDSANALYTICS) {

        nlohmann::json payload;

        auto analytics_meta =
            static_cast<NvDsAnalyticsFrameMeta *>(user_meta->user_meta_data);

        if (!analytics_meta) {
          continue;
        }
        /*
           Generate the areas.
           */
        std::vector<nlohmann::json> areas;
        for (const auto &[name, count] : analytics_meta->objInROIcnt) {
          areas.push_back(nlohmann::json{{"name", name}, {"count", count}});
        }
        payload["areas"] = areas;
        /*
           Generate the lines.
           */
        std::vector<nlohmann::json> lines;
        for (const auto &[name, count] : analytics_meta->objLCCurrCnt) {
          lines.push_back(nlohmann::json{{"name", name}, {"count", count}});
        }
        payload["lines"] = lines;
        /*
           Add the payload to the batch.
           */
        payloads.push_back(nlohmann::json{
            {"areas", areas}, {"lines", lines}, {"timestamp", timestamp}});
      }
    }
  }
  return payloads;
}
