#include <viflow/element.hxx>

#include "scout/decode_bin_pad_linker.hxx"

Scout::Decode_Bin_Pad_Linker::Decode_Bin_Pad_Linker(
    viflow::Shared_Element &video_target)
    : video_elem{video_target} {}

void Scout::Decode_Bin_Pad_Linker::process_pad(viflow::Shared_Element &elem,
                                               viflow::Pad &new_pad) {

  auto caps = new_pad.query_caps();
  auto str = caps.get_structure(0);
  auto name = gst_structure_get_name(str);

  if (g_str_has_prefix(name, "video/x-raw")) {
    new_pad.link(video_elem->get_pad("sink"));
  }
}
