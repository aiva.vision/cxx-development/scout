#include <gst/gst.h>
#include <gst/gstevent.h>
#include <spdlog/spdlog.h>
#include <viflow/main_loop.hxx>

#include "scout/pipeline.hxx"

GST_DEBUG_CATEGORY_STATIC(scout);
#define GST_CAT_DEFAULT scout

void stop(int signal_number) {

  spdlog::info("Caught signal with ID {}, stopping the pipeline...",
               signal_number);
  /*
     Send an EOS event and let the bus listener handle it.
     */
  Scout::Pipeline::get_instance()->stop();
  viflow::Main_Loop::get_instance()->quit();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer.
     */
  gst_init(0, nullptr);
  GST_DEBUG_CATEGORY_INIT(scout, "Scout", 0, "Scout debug category.");
  /*
     Make sure a config is passed.
     */
  if (argc != 2) {
    spdlog::error("Please provide a config!");
  }
  auto pipeline = Scout::Pipeline::register_instance(argv[1]);
  /*
     Set the pipeline to PLAYING.
     */
  pipeline->play();
  /*
     Run the main loop.
     */
  auto &main_loop = viflow::Main_Loop::register_instance();
  main_loop->run();
  /*
     Leave the rest to RAII.
     */
  spdlog::info("Exited gracefully.");
  return EXIT_SUCCESS;
}
