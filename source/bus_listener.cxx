#include <cstdlib>
#include <gst/gstbin.h>
#include <gst/gstdebugutils.h>
#include <spdlog/spdlog.h>
#include <viflow/main_loop.hxx>

#include "scout/bus_listener.hxx"
#include "scout/pipeline.hxx"

bool Scout::Bus_Listener::process_message(const viflow::Bus &bus,
                                          const viflow::Message &message) {

  switch (message.get_type()) {
  case GST_MESSAGE_INFO:
    process_info(message);
    break;
  case GST_MESSAGE_WARNING:
    process_warning(message);
    break;
  case GST_MESSAGE_ERROR:
    process_error(message);
    break;
  case GST_MESSAGE_STATE_CHANGED:
    process_state_changed(message);
    break;
  case GST_MESSAGE_EOS:
    process_eos(message);
    break;
  default:
    break;
  }
  return true;
}

void Scout::Bus_Listener::process_info(const viflow::Message &message) {

  auto context = message.get_info_context();
  spdlog::info("Received a message from the '{}' element with the content '{}'",
               message.get_source_name(), context.get_error_content());
}

void Scout::Bus_Listener::process_warning(const viflow::Message &message) {

  auto context = message.get_warning_context();
  spdlog::warn("Received a message from the '{}' element with the content '{}'",
               message.get_source_name(), context.get_error_content());
}

void Scout::Bus_Listener::process_error(const viflow::Message &message) {

  auto context = message.get_error_context();
  spdlog::error(
      "Received a message from the '{}' element with the content '{}'",
      message.get_source_name(), context.get_error_content());

  Pipeline::get_instance()->stop();
  viflow::Main_Loop::get_instance()->quit();
}

void Scout::Bus_Listener::process_state_changed(
    const viflow::Message &message) {

  if (message.get_source_name() == Pipeline::name) {

    auto pipeline = Pipeline::get_instance();
    auto context = message.get_state_changed_context();

    switch (context.get_new_state()) {
    case GST_STATE_PLAYING:
      spdlog::info("The pipeline is playing.");
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline->get_core()),
                                GST_DEBUG_GRAPH_SHOW_ALL, "scout-playing");
      break;
    case GST_STATE_PAUSED:
      if (context.get_old_state() == GST_STATE_PLAYING) {
        spdlog::info("The pipeline is paused.");
      }
      break;
    case GST_STATE_READY:
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline->get_core()),
                                GST_DEBUG_GRAPH_SHOW_ALL, "scout-ready");
      if (context.get_old_state() == GST_STATE_NULL) {
        spdlog::info("The pipeline is ready.");
      } else {
        spdlog::info("The pipeline is stopped.");
      }
      break;
    default:
      break;
    }
  }
}

void Scout::Bus_Listener::process_eos(const viflow::Message &message) {

  spdlog::info("End-Of-Stream reached.");

  Pipeline::get_instance()->stop();
  viflow::Main_Loop::get_instance()->quit();
}
