#include <viflow/queue.hxx>
#include <viflow/uri_decode_bin.hxx>

#include "scout/decode_bin_child_cb.hxx"
#include "scout/decode_bin_pad_linker.hxx"
#include "scout/source.hxx"

Scout::Source::Source(const std::string &name) : Bin{name} {
  /*
     Start from the second element from the bin because we need to refernce it
     for the decodebin.
     */
  auto queue = add<viflow::Queue>("converter_queue");
  auto converter = append("nvvideoconvert", "video_converter");

  converter->set_property("nvbuf-memory-type", 0);
  /*
     Now we will tell the decode bin which is the element we have to connect to.
     After necessary elements are plugged together( after the pipeline starts ).
     */
  auto decode_bin = add<viflow::URI_Decode_Bin>("uri_decode_bin");

  decode_bin->cast<viflow::URI_Decode_Bin>()->connect_callback(
      viflow::Child_Proxy_Signal::child_added,
      std::make_unique<Decode_Bin_Child_Cb>());

  decode_bin->cast<viflow::URI_Decode_Bin>()->connect_callback(
      std::make_unique<Decode_Bin_Pad_Linker>(queue));
  /*
     Finally set the appropriate ghost pads.
     */
  set_src_elem("video_converter");
}

void Scout::Source::set_uri(const std::string &uri) {
  get_element("uri_decode_bin")->set_property("uri", uri.c_str());
}
