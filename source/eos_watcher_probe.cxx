#include <spdlog/spdlog.h>
#include <viflow/main_loop.hxx>

#include "scout/eos_watcher_probe.hxx"
#include "scout/pipeline.hxx"

GstPadProbeReturn Scout::EOS_Watcher_Probe::process(GstPad *pad,
                                                    GstPadProbeInfo *info) {

  GstEvent *event = static_cast<GstEvent *>(info->data);

  if (GST_EVENT_TYPE(event) == GST_EVENT_EOS) {

    spdlog::warn("Received an EOS event from the source, attempting to reset.");

    auto pipeline = Pipeline::get_instance();
    pipeline->cast<Scout::Pipeline>()->reset_source();
    /*
       Finally, drop the eos event.
       */
    return GST_PAD_PROBE_DROP;
  }
  return GST_PAD_PROBE_OK;
}

GstPadProbeType Scout::EOS_Watcher_Probe::get_type() const noexcept {

  return GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM;
}
